<%-- 
    Document   : index
    Created on : 03-07-2021, 22:49:37
    Author     : acamposm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>URL  API  MASCOTAS</h1>
        <h2> *   Lista de Mascotas - GET - http://evaluacion03andreacampos.herokuapp.com/api/mascotas </h2>
        <h2> *   Modificación de Mascotas - POST -  http://evaluacion03andreacampos.herokuapp.com/api/mascotas </h2>
        <h2> *   Creación de Mascotas - PUT -  http://evaluacion03andreacampos.herokuapp.com/api/mascotas </h2>
        <h2> *   Eliminación de Mascotas - DELETE -  http://evaluacion03andreacampos.herokuapp.com/api/mascotas/{id(rutduenio)} </h2>
        <h2> *   Consulta por Mascotas - GET por ID - http://evaluacion03andreacampos.herokuapp.com/api/mascotas/{id(rutduenio)} </h2>
        
    </body>
</html>
