/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.examen03andreacampos.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author acamposm
 */
@Entity
@Table(name = "mascotas")
@NamedQueries({
    @NamedQuery(name = "Mascotas.findAll", query = "SELECT m FROM Mascotas m"),
    @NamedQuery(name = "Mascotas.findByRutduenio", query = "SELECT m FROM Mascotas m WHERE m.rutduenio = :rutduenio"),
    @NamedQuery(name = "Mascotas.findByNombreduenio", query = "SELECT m FROM Mascotas m WHERE m.nombreduenio = :nombreduenio"),
    @NamedQuery(name = "Mascotas.findByTelefono", query = "SELECT m FROM Mascotas m WHERE m.telefono = :telefono"),
    @NamedQuery(name = "Mascotas.findByDireccion", query = "SELECT m FROM Mascotas m WHERE m.direccion = :direccion"),
    @NamedQuery(name = "Mascotas.findByNombremascota", query = "SELECT m FROM Mascotas m WHERE m.nombremascota = :nombremascota")})
public class Mascotas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rutduenio")
    private String rutduenio;
    @Size(max = 2147483647)
    @Column(name = "nombreduenio")
    private String nombreduenio;
    @Size(max = 2147483647)
    @Column(name = "telefono")
    private String telefono;
    @Size(max = 2147483647)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 2147483647)
    @Column(name = "nombremascota")
    private String nombremascota;

    public Mascotas() {
    }

    public Mascotas(String rutduenio) {
        this.rutduenio = rutduenio;
    }

    public String getRutduenio() {
        return rutduenio;
    }

    public void setRutduenio(String rutduenio) {
        this.rutduenio = rutduenio;
    }

    public String getNombreduenio() {
        return nombreduenio;
    }

    public void setNombreduenio(String nombreduenio) {
        this.nombreduenio = nombreduenio;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNombremascota() {
        return nombremascota;
    }

    public void setNombremascota(String nombremascota) {
        this.nombremascota = nombremascota;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rutduenio != null ? rutduenio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mascotas)) {
            return false;
        }
        Mascotas other = (Mascotas) object;
        if ((this.rutduenio == null && other.rutduenio != null) || (this.rutduenio != null && !this.rutduenio.equals(other.rutduenio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.examen03andreacampos.entity.Mascotas[ rutduenio=" + rutduenio + " ]";
    }
    
}
