/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.examen03andreacampos.services;

import com.mycompany.examen03andreacampos.dao.MascotasJpaController;
import com.mycompany.examen03andreacampos.dao.exceptions.NonexistentEntityException;
import com.mycompany.examen03andreacampos.entity.Mascotas;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author acamposm
 */
@Path("mascotas")
public class MascotasRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarMascotas() {

        MascotasJpaController dao = new MascotasJpaController();

        List<Mascotas> mascotas = dao.findMascotasEntities();

        return Response.ok(200).entity(mascotas).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearMascota(Mascotas mascotas) {

        MascotasJpaController dao = new MascotasJpaController();

        try {
            dao.create(mascotas);
        } catch (Exception ex) {
            Logger.getLogger(MascotasRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(mascotas).build();

    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarMascota(Mascotas mascotas) {
        MascotasJpaController dao = new MascotasJpaController();
        try {
            dao.edit(mascotas);
        } catch (Exception ex) {
            Logger.getLogger(MascotasRest.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        return Response.ok(200).entity(mascotas).build();
    }
    
    @DELETE
    @Path("/{ideliminar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminarMascota(@PathParam("ideliminar") String ideliminar) {
         MascotasJpaController dao = new MascotasJpaController();
        try {
            dao.destroy(ideliminar);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(MascotasRest.class.getName()).log(Level.SEVERE, null, ex);
        }
         
          return Response.ok("Registro de mascota eliminado!").build();
    }
    
    @GET
    @Path("/{idconsultar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarPorMascota(@PathParam("idconsultar") String idconsultar) {
          MascotasJpaController dao = new MascotasJpaController();
          
         Mascotas mascota= dao.findMascotas(idconsultar);
         
         return Response.ok(200).entity(mascota).build();
    }
    

}
